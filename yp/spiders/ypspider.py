from yp.items import YpItem
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
import random
from yp.proxies import *
import urlparse
from scrapy import log
import re
from yp.settings import CITY_FILE_NAME, CATEGORY_FILE_NAME

from scrapy.utils.project import get_project_settings
from scrapy.contrib.exporter import CsvItemExporter
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import TakeFirst, MapCompose

class CSVkwItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        kwargs['fields_to_export'] = [
    'business_name',
    'address',
    'phone',
    'website',
    'email_business',
    'view_services',
    'see_our_services',
    'hours',
    'general_info',
    'services_products',
    'aka',
    'brands',
    'payment_method',
    'location',
    'languages',
    'other_link',
    'accreditation',
    'amenities',
    'extra_phones',
    'neighborhoods',
    'categories',
    'gallery',
    'reviews',
    'social_links',
    'url',
    ]
        kwargs['encoding'] = 'utf-8'

        super(CSVkwItemExporter, self).__init__(*args, **kwargs)

class YpLoader(XPathItemLoader):
    default_input_processor = MapCompose(lambda s: re.sub('\s+', ' ', s.strip()))
    default_output_processor = TakeFirst()


class ProxyMiddleware(object):

    # overwrite process request
    def process_request(self, request, spider):
        proxy = random.choice(PROXIES)
        request.meta['proxy'] = "http://" + proxy

class RandomUserAgent(object):
    """Selects a user agent at random from the list specifed by the USER_AGENTS
    setting to use for each request."""

    def __init__(self, user_agents):
        # Disable this middleware if user_agents is invalid, by raising
        # NotConfigured.
        try:
            random.choice(user_agents)
        except:
            raise "Error"

        self.user_agents = user_agents

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings.get('USER_AGENTS'))

    def process_request(self, request, spider):
        user_agent = random.choice(self.user_agents)
        request.headers['User-Agent'] = user_agent

class YpSpider(BaseSpider):
    name = "yp"

    def __init__(self, *args, **kwargs):
        self.allowed_domains = ["yellowpages.com"]
        self.start_urls = ["http://www.yellowpages.com"]
        #self.start_urls = ["http://www.yellowpages.com/north-bergen-nj/child-care"]
        settings = get_project_settings()
        super(YpSpider, self).__init__(*args, **kwargs)


    def parse(self, response):
        city_count = 0
        with open(CITY_FILE_NAME) as f:
            city_count = len(f.readlines())

        category_count = 0
        with open(CATEGORY_FILE_NAME) as f:
            category_count = len(f.readlines())


        for cat_start in range(0, category_count, 10):
            for city_start in range(0, city_count, 20):
                yield Request('http://www.yellowpages.com/whitepages',
                              meta={'city_start':city_start, 'cat_start':cat_start}, 
                              callback=self.fake_parse,
                              dont_filter=True)


    def fake_parse(self, response):
        cities = []
        with open(CITY_FILE_NAME) as f:
            cities = f.readlines()

        categories = []
        with open(CATEGORY_FILE_NAME) as f:
            categories = f.readlines()

        city_start = response.meta['city_start']
        cat_start = response.meta['cat_start']
        for category in categories[cat_start:cat_start + 10]:
            for city in cities[city_start:city_start + 20]:
                link = 'http://www.yellowpages.com/%s%s' % (city.strip(), category.strip())
                yield Request(link.strip(), callback=self.parse_category)



    def parse_category(self, response):
        hxs = HtmlXPathSelector(response)

        item_links = hxs.select("//a[@class='business-name']/@href").extract()
        
        if not item_links:
                log.msg('url has no items: ' + response.url, log.INFO)

        for link in item_links:
            yield Request(urlparse.urljoin(response.url, link), self.parse_item)

        next_page = hxs.select("//a[@class='next ajax-page']/@data-page").extract()
        if next_page:
            url = response.url.split("?")[0] + '?page=' + next_page[0]
            yield Request(url, self.parse_category)

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)

        item = YpItem()
        l = YpLoader(YpItem(), hxs)

        l.add_xpath('business_name', '//div[@class="business-card-wrapper"]//*[@itemprop="name"]/text()')
        l.add_xpath('address', '//p[@class="street-address" or @class="city-state"]/text()')
        l.add_xpath('phone', '//p[@class="phone"]/text()')
        l.add_xpath('website', '//a[@class="custom-link" and text()="Visit Website"]/@href')
        l.add_xpath('email_business', '//a[@class="email-business"]/@href')
        l.add_xpath('view_services', '//a[@class="view-services"]/@href')
        l.add_xpath('see_our_services', '//a[@class="custom-link" and text()="See Our Services"]/@href')
        l.add_xpath('hours', '//time/@datetime | //div[@id="special-hours"]//text()')
        l.add_xpath('general_info', '//dd[preceding-sibling::dt="General Info:"][1]//text()')
        l.add_xpath('services_products', '//dd[preceding-sibling::dt="Services/Products:"][1]//text()')
        l.add_xpath('aka', '//section[@id="business-details"]//dd[@class="aka"]//p//text()')
        l.add_xpath('brands', '//section[@id="business-details"]//dd[@class="brands"]//text()')
        l.add_xpath('payment_method', '//section[@id="business-details"]//dd[@class="payment"]//text()')
        l.add_xpath('location', '//section[@id="business-details"]//dd[@class="location-description"]//text()')
        l.add_xpath('languages', '//section[@id="business-details"]//dd[@class="languages"]//text()')
        l.add_xpath('other_link', '//section[@id="business-details"]//dd[@class="weblinks"]//text()')
        l.add_xpath('accreditation', '//section[@id="business-details"]//dd[@class="accreditation"]/p/text()')
        l.add_xpath('amenities', '//section[@id="business-details"]//dd[@class="amenities"]//text()')
        l.add_xpath('extra_phones', '//section[@id="business-details"]//dd[@class="extra-phones"]//span/text()')
        l.add_xpath('neighborhoods', '//section[@id="business-details"]//dd[@class="neighborhoods"]//a//text()')
        l.add_xpath('categories', '//section[@id="business-details"]//dd[@class="categories"]//a//text()')
        l.add_xpath('gallery', '//section[@id="photos"]//footer//a/@href')
        l.add_xpath('reviews', '//div[@id="reviews-container"]//div[@itemtype="http://schema.org/Review"]')
        l.add_xpath('social_links', '//dd[preceding-sibling::dt="Social Links:"][1]/a/@href')
        l.add_value('url', response.url)

        yield l.load_item()

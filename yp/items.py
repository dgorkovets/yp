# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader.processor import TakeFirst, Join


class YpItem(Item):
    business_name = Field()
    address = Field(output_processor=Join())
    phone = Field()
    website = Field()
    email_business = Field()
    view_services = Field()
    see_our_services = Field()
    hours = Field(output_processor=Join('$'))
    general_info = Field()
    services_products = Field(output_processor=Join())
    aka = Field(output_processor=Join('; '))
    brands = Field()
    payment_method = Field()
    location = Field()
    languages = Field()
    other_link = Field(output_processor=Join(', '))
    accreditation = Field(output_processor=Join(', '))
    amenities = Field()
    extra_phones = Field(output_processor=Join())
    neighborhoods = Field(output_processor=Join(', '))
    categories = Field(output_processor=Join(', '))
    gallery = Field()
    reviews = Field(output_processor=Join('[review-separator]'))
    social_links = Field(output_processor=Join(', '))
    url = Field()


DOWNLOAD_DELAY = 0.4
CITY_FILE_NAME = 'YP_cities.txt' 
CATEGORY_FILE_NAME = 'YC_categories.txt'
LOG_LEVEL = 'INFO'
RETRY_TIMES = 5
FEED_FORMAT = 'csv'




BOT_NAME = 'yp'

SPIDER_MODULES = ['yp.spiders']
NEWSPIDER_MODULE = 'yp.spiders'

FEED_EXPORTERS = {
    'csv': 'yp.spiders.ypspider.CSVkwItemExporter',
}

DOWNLOADER_MIDDLEWARES = {
    'yp.spiders.ypspider.ProxyMiddleware': 100,
    'yp.spiders.ypspider.RandomUserAgent' : 1001,
    }

ITEM_PIPELINES = {
    'yp.pipelines.YpPipeline': 300,
}


USER_AGENTS = [
    # Chrome 31.0
    ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
     'Chrome/31.0.1650.4 Safari/537.36'),
    # Firefox 25.0
    ('Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 '
     'Firefox/25.0'),
    # Safari 5.1.7
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ '
     '(KHTML, like Gecko) Version/5.1.7 Safari/534.57.2'),
    ('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 '
     '(KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36'),
    ('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) '
     'Gecko/20100101 Firefox/23.0'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/536.30.1 '
     '(KHTML, like Gecko) Version/6.0.5 Safari/536.30.1'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 '
     '(KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) '
     'Gecko/20100101 Firefox/23.0'),
    ('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 '
     '(KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36'),
    ('Mozilla/5.0 (Windows NT 5.1; rv:15.0) '
     'Gecko/20100101 Firefox/13.0.1'),
    ('Mozilla/5.0 (Windows NT 6.1; rv:12.0) '
     'Gecko/20120403211507 Firefox/12.0'),
    ('Mozilla/5.0 (Windows NT 5.1; rv:15.0) '
     'Gecko/20100101 Firefox/13.0.1'),
    ('Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; '
     'WOW64; Trident/6.0)'),
    ('Mozilla/5.0 (X11; Arch Linux i686; rv:2.0) '
     'Gecko/20110321 Firefox/4.0'),
    ('Mozilla/5.0 (Windows; U; MSIE 9.0; '
     'Windows NT 9.0; en-US))'),
    ('Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20110319 '
     'Firefox/4.0'),
    ('Mozilla/5.0 (Windows NT 6.1; rv:1.9) Gecko/20100101 '
     'Firefox/4.0'),
    ('Opera/9.20 (Windows NT 6.0; U; en)'),
    ('Opera/9.00 (Windows NT 5.1; U; en)'),
    ('Opera/9.64(Windows NT 5.1; U; en) Presto/2.1.1'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.22 '
     '(KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.13 '
     '(KHTML, like Gecko) Chrome/24.0.1290.1 Safari/537.13'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 '
     '(KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17'),
    ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ '
     '(KHTML, like Gecko) Version/5.1.7 Safari/534.57.2'),
    ('Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; ja-jp) '
     'AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'),
    ('Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) '
     'Gecko/20121011 Firefox/16.0.1'),
    ('Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:15.0) '
     'Gecko/20100101 Firefox/15.0.1'),
    ('Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0) '
     'Opera 12.14')
]


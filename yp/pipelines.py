from scrapy.exceptions import DropItem
from scrapy.http import Request
from urlparse import urljoin
import json
from lxml import etree
from StringIO import StringIO

class YpPipeline(object):
    def process_item(self, item, spider):
        item['email_business'] = item.get('email_business', '').lstrip('mailto:')
        if item.get('gallery', ''):
            item['gallery'] = urljoin(item['url'], item['gallery'])

        if item.get('hours', ''):
            hours = item['hours'].replace('$Closed', ' Closed')
            hours = hours.replace('Mon', 'Mo')\
                         .replace('Tue', 'Tu')\
                         .replace('Wed', 'We')\
                         .replace('Thu', 'Th')\
                         .replace('Fri', 'Fr')\
                         .replace('Sat', 'Sa')\
                         .replace('Sun', 'Su')\
                         .replace (' - ', '-')

            hours = hours.split('$')
            hour_dict = {}
            for day in hours:
                tokens = day.split(' ', 1)
                dov = tokens[0]
                h = 'Open 24 Hours'
                if len(tokens) > 1:
                    h = tokens[1]
                hour_dict[dov] = h
            item['hours'] = json.dumps(hour_dict)


        if item.get('reviews', ''):
            review_list = []
            reviews = item['reviews'].split('[review-separator]')
            for review in reviews:
                review_elem = {}
                root = etree.parse(StringIO(review), etree.XMLParser(recover=True))
                review_elem['rating'] = ''.join(root.xpath('.//meta[@itemprop="ratingValue"]/@content'))
                review_elem['author'] = ''.join(root.xpath('.//a[@itemprop="author"]/text()'))
                review_elem['date'] = ''.join(root.xpath('.//span[@itemprop="datepublished"]/text()'))
                review_elem['title'] = ''.join(root.xpath('.//header[@class="review-title"]/text()'))
                review_elem['body'] = ''.join(root.xpath('.//p[@itemprop="reviewBody"]//span[not(@class="ellipsis")]//text()'))

                # if body is short
                if not review_elem['body']:
                    review_elem['body'] = ''.join(root.xpath('.//p[@itemprop="reviewBody"]//text()'))

                review_list.append(review_elem)
            item['reviews'] = json.dumps(review_list)
        

        return item

